package fr.exercice.kata.model;

import java.util.ArrayList;
import java.util.List;

public class StringCalculator {

    private boolean isNegativeNumber = false;
    private List<Integer> negativeNumbers = new ArrayList<>();


    public int add(String numbers) {
        int result = 0;
        String[] numberList = splitList(numbers);

        for (String number: numberList) {
            result += convertPositiveNumberToInt(number);
        }

        if (isNegativeNumber)
            throw new IllegalArgumentException("negatives not allowed "+ negativeNumbers.toString());

        return result;
    }


    private String[] splitList(String str){
        if (str.isEmpty()){
            return new  String[]{""};
        }
        else {
            String[] splitList = str.substring(2).split("[\n]", 2);
            String delimiter = "[" + splitList[0] + "\n]";
            return splitList[1].split(delimiter);
        }
    }

    private int convertPositiveNumberToInt(String str){
        if (str.matches("-?\\d+")) {
            if (Integer.parseInt(str)<0) {
                isNegativeNumber((Integer.parseInt(str)));
                return 0;
            }
            else
                return Integer.parseInt(str);
        }
        return 0;
    }

    private void isNegativeNumber(int number){
        isNegativeNumber = true;
        negativeNumbers.add(number);
    }

}