package fr.exercice.kata;

import fr.exercice.kata.model.StringCalculator;

public class StringCalculatorKataApp {

    public static void main(String[] args) {

        StringCalculator stringCalculator = new StringCalculator();
        String input = "//,\n1,2,3\n5,8\n12,58,99";

        System.out.println("Resultat : " + stringCalculator.add(input));
    }

}
