package fr.exercice.kata.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringCalculatorTest {

    private StringCalculator stringCalculator;

    @BeforeEach
    public void initStringCalculator() {
        stringCalculator = new StringCalculator();
    }


    @Test
    public void inputEmptyRetrun0(){
        assertEquals(0, stringCalculator.add(""));
    }

    @Test
    public void input1NumberRetrunNumber(){
        assertEquals(1, stringCalculator.add("//;\n1"));
    }

    @Test
    public void input2NumberRetrunSumm(){
        assertEquals(3,stringCalculator.add("//;\n1;2"));
    }

    @Test
    public void inputLetterRetrun0instead(){
        assertEquals(1,stringCalculator.add("//;\n1;A"));
    }

    @Test
    public void input6NumberAndSemicolonDelimiterRetrunSumm(){
        assertEquals(31, stringCalculator.add("//;\n1;2;3;5;8;12"));
    }

    @Test
    public void input6NumberAndCommaAndNewLineRetrunSumm(){
        assertEquals(31, stringCalculator.add("//,\n1,2,3\n5,8\n12"));
    }

    @Test
    public void input1NumberSlashDelimiterNewLineRetrunSumm(){
        assertEquals(1, stringCalculator.add("///\n1/\n"));
    }

    @Test
    public void inputNegativeNumberAndCommaAndNewLineRetrunSumm(){
        IllegalArgumentException thrown =
                assertThrows(IllegalArgumentException.class
                        , () -> stringCalculator.add("//,\n1,-2,3\n5,-8\n12"));

        assertEquals("negatives not allowed [-2, -8]", thrown.getMessage());
    }
}